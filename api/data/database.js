const { Pool } = require("pg");

// DOCUMENTATION : https://node-postgres.com/api/pool

class dataBase {
  /**
   * Classe permettant de gérer les instances de la base de données.
   */

  constructor() {}

  /**
   * Si aucune instance de la base de données n'est disponible, on en crée une nouvelle
   * @return {object} retourne une instance disponible de la base de données
   */
  static getDB() {
    if (typeof dataBase.db === "undefined") {
      dataBase.initDB();
    }
    return dataBase.db;
  }

  /**
   * Crée une instance de la base de données
   * @return {void}
   */
  static initDB() {
    dataBase.db = new Pool({
      host: process.env.DB_HOST || "localhost",
      port: process.env.DB_PORT || 5432,
      user: process.env.DB_USER || "remini",
      password: process.env.DB_PASSWORD || "",
      database: process.env.DB_NAME || "sinan",
      max: 20,
      connectionTimeoutMillis: 0,
      idleTimeoutMillis: 0,
    });
  }
}

module.exports = dataBase;
