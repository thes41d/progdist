const express = require("express");
const { authenticateToken } = require("../middlewares/token");
const router = express.Router();
const {
  getProject,
  getProjects,
  removeProject,
  addProject,
  recordProject,
} = require("../controllers/projects");

router.use("/", authenticateToken);
router.use("/project/:id", authenticateToken);

/**
 * @swagger
 * /projects/:
 *    get:
 *      security:
 *        - Bearer: []
 *      description: Get all the projects
 *      responses:
 *        200:
 *          description: Success
 *        403:
 *          description: Unauthorized
 *        500:
 *          description: DB Error
 *
 */

/**
 * @swagger

 * /projects/:
 *    post:
 *      security: 
 *        - Bearer: []
 *      description: Add a new project
 *      parameters:
 *      - name: name
 *        description: Project name
 *        in: formData
 *        required: true
 *      
 *      responses:
 *        200:
 *          description: Success
 *        403:
 *          description: Unauthorized
 *        500:
 *          description: DB error
 *    
 */

router
  .route("/")
  .get((req, res) => {
    const userID = req.user.id;
    getProjects(userID, res);
  })
  // Add the project to the db from the name and return new project created
  .post((req, res) => {
    const projectName = req.body.name;
    const userID = req.user.id;
    addProject(projectName, userID, res);
    getProjects(userID, res);
  });

/**
 * @swagger
 * /projects/project/{id}:
 *    get:
 *      security:
 *        - Bearer: []
 *      description: Get a project from it's id
 *      responses:
 *        200:
 *          description: Success
 *        403:
 *          description: Unauthorized
 *        500:
 *          description: DB Error
 *
 */

/**
 * @swagger
 * /projects/project/{id}:
 *    put:
 *      security:
 *        - Bearer: []
 *      description: Change the dataset and the features of an existing project
 *      parameters:
 *        - name: dataset
 *          description: The dataset
 *          in: formData
 *          required: true
 *        - name: xfeature
 *          description: Feature to plot on the x-axis
 *          in: formData
 *          required: true
 *        - name: yfeature
 *          description: Feature to plot on the y-axis
 *          in: formData
 *          required: true
 *      responses:
 *        200:
 *          description: Success
 *        403:
 *          description: Unauthorized
 *        500:
 *          description: DB Error
 *
 */

/**
 * @swagger
 * /projects/project/{id}:
 *    delete:
 *      security:
 *        - Bearer: []
 *      description: remove a project based on its id
 *      responses:
 *        200:
 *          description: Success
 *        403:
 *          description: Unauthorized
 *        500:
 *          description: DB Error
 *
 */

router
  .route("/project/:id")

  .get((req, res) => {
    const userID = req.user.id;
    const projectID = req.params.id;
    getProject(userID, projectID, res);
  })

  .put((req, res) => {
    const userID = req.user.id;
    const projectID = req.params.id;
    recordProject(userID, projectID, req.body, res);
  })

  .delete((req, res) => {
    const userID = req.user.id;
    const projectID = req.params.id;
    removeProject(projectID, userID, res);
  });

module.exports = router;
