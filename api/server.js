const express = require("express");
require("dotenv").config();
const app = express();
const bodyParser = require("body-parser");
var cors = require("cors"); // authorize all cors
const { addUser } = require("./controllers/register");
const { authUser } = require("./controllers/login");
const { authenticateToken } = require("./middlewares/token");
const { getMail } = require("./controllers/getMail");
const { deleteAccount } = require("./controllers/deleteAccount");
const { registerValidation } = require("./middlewares/inputValidation");
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUI = require("swagger-ui-express");
require("./data/database").initDB();

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "DataBeez API",
      version: "1.0.0",
    },
  },

  apis: ["./routes/projects.js", "server.js"],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerDocs));

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());
app.use(cors());

app.use("/projects", require("./routes/projects"));

/**
 * @swagger
 * /register:
 *
 *    post:
 *      description: Create a new account
 *      parameters:
 *        - name: mail
 *          description: User's mail
 *          in: formData
 *          required: true
 *        - name: password
 *          description: User's password
 *          in: formData
 *          required: true
 *        - name: passwordVerify
 *          description: User's password
 *          in: formData
 *          required: true
 *      responses:
 *        200:
 *          description: Success
 *        400:
 *          description: Bad request
 *        500:
 *          description: DB Error
 */

app.post("/register", registerValidation, (req, res) => {
  const userInfo = req.body;
  addUser(userInfo, res);
});

/**
 * @swagger
 * /login:
 *    post:
 *      description: Log In
 *      parameters:
 *        - name: mail
 *          description: User's mail
 *          in: formData
 *          required: true
 *        - name: password
 *          description: User's password
 *          in: formData
 *          required: true
 *      responses:
 *        200:
 *          description: Success
 *        401:
 *          description: Bad Request
 *        500:
 *          description: Internal Error
 */
app.post("/login", (req, res) => {
  const userInfo = req.body;
  authUser(userInfo, res);
});
/**
 * @swagger
 * /getmail:
 *    get:
 *      security:
 *        - Bearer: []
 *      description: Get the mail of the connected user
 *      responses:
 *        200:
 *          description: Success
 *        403:
 *          description: Bad Request
 *        500:
 *          description: Internal Error
 */
app.get("/getmail", authenticateToken, (req, res) => {
  const userID = req.user.id;
  getMail(userID, res);
});

/**
 * @swagger
 * /deleteaccount:
 *    delete:
 *      security:
 *        - Bearer: []
 *      description: Delete the account of the connected user
 *      responses:
 *        200:
 *          description: Success
 *        403:
 *          description: Bad Request
 *        500:
 *          description: Internal Error
 */
app.delete("/deleteaccount", authenticateToken, async (req, res) => {
  const userID = req.user.id;
  deleteAccount(userID, res);
});

/**
 * @swagger
 * /test:
 *    get:
 *      description: test if the api is running
 *      responses:
 *        200:
 *          description: Success
 */

app.get("/test", (req, res) => {
  res.end("API running");
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log("Listening to port " + PORT);
});

module.exports = app;
