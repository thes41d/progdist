const { getUser } = require("../helpers/getUser");
const { checkPass } = require("../helpers/password/checkPass");
const { generateAccessToken } = require("../middlewares/token");

/**
 * Fonction d'authentification de l'utilisateur
 * @param {object} user - objet contenant le mail et le mot de passe de l'utilisateur
 * @param {object} res - réponse HTTP
 * @returns {object}  réponse HTTP contenant soit une erreur soit un token
 */
const authUser = async (user, res) => {
  let infoDB;
  // Requete à la BDD pour récupérer les infos de l'utilisateur grâce au mail
  try {
    infoDB = await getUser(user.mail);
  } catch (err) {
    return res.status(500).json({
      error: "Error DB",
    });
  }

  // Si l'utilisateur n'est pas inscrit (Si la db renvoie 0 ligne) on revoie un msg d'erreur
  if (!infoDB.rowCount) {
    return res.status(401).json({
      error: "Incorrect mail or password", // Ne pas renvoyer "Vous n'êtes pas inscrit" => Evite à un tier de savoir que ce mail est stocké dans la BDD
    });
  }
  // Si il est inscrit,  on peux effectuer le traitement des infos recu

  let userDBHashPass = infoDB.rows[0].password; // Recupere le mot de passe haché de la bdd dans la variable userDBHashPass
  let match = await checkPass(user.password, userDBHashPass); // Renvoie true si les mots de passe coincident false sinon

  if (match) {
    // Si tout se passe correctement , on renvoie un JWT à l'utilisateur
    let userID = infoDB.rows[0].id;
    return res.status(200).json({
      token: generateAccessToken({ id: userID }),
    });
  }
  // Si les mots de passes ne "match" pas on revoie un message d'erreur
  return res.status(401).json({
    error: "Incorrect mail or password",
  });
};

exports.authUser = authUser;
