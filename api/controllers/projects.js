const db = require("../data/database");

const addProject = async (projectName, userID, res) => {
  const reqDB = {
    text: "INSERT INTO PROJECTS(userID,name) VALUES($1,$2)",
    values: [userID, projectName],
  };

  try {
    await db.getDB().query(reqDB);
  } catch (err) {
    return res.status(500).json({
      error: "An error occured while adding your project to our database",
    });
  }
};

const recordProject = async (userID, projectID, body, res) => {
  const { dataset, xfeature, yfeature } = body;
  const reqDB = {
    text: "UPDATE PROJECTS SET dataset = $1 ,xfeature = $2 , yfeature = $3 WHERE userid = $4 AND projectid = $5",
    values: [dataset, xfeature, yfeature, userID, projectID],
  };

  try {
    await db.getDB().query(reqDB);
  } catch (err) {
    return res.status(500).json({
      error: "An error occured while saving your project",
    });
  }
  return res.status(201).json({
    message: "The project has been updated successfully",
  });
};

const removeProject = async (projectID, userID, res) => {
  const reqDB = {
    text: "DELETE FROM PROJECTS WHERE userID = $1 AND projectID = $2",
    values: [userID, projectID],
  };
  try {
    await db
      .getDB()
      .query(reqDB)
      .then((result) => result.rows);
  } catch (err) {
    return res.status(500).json({
      error: "An error occured while deleting your project",
    });
  }
  return res.status(200).json({
    message: "The project has been successfully removed",
  });
};
const getProject = async (userID, projectID, res) => {
  let project;
  const reqDB = {
    text: "SELECT dataset,name,projectID,xfeature,yfeature FROM PROJECTS WHERE userID = $1 AND projectID = $2",
    values: [userID, projectID],
  };
  try {
    project = await db
      .getDB()
      .query(reqDB)
      .then((result) => result.rows[0]);
  } catch (err) {
    return res.status(500).json({ error: "Error DB" });
  }
  return res.status(200).json({
    project,
  });
};
const getProjects = async (userID, res) => {
  const reqDB = {
    text: "SELECT dataset,name,projectID FROM PROJECTS WHERE userID = $1",
    values: [userID],
  };
  let projects;
  try {
    projects = await db
      .getDB()
      .query(reqDB)
      .then((result) => result.rows);
  } catch (err) {
    return res.status(500).json({ error: "Error DB" });
  }

  return res.status(200).json({
    projects,
  });
};

exports.getProjects = getProjects;
exports.getProject = getProject;
exports.removeProject = removeProject;
exports.addProject = addProject;
exports.recordProject = recordProject;
