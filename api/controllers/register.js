const { hashPass } = require("../helpers/password/hashPass");
const db = require("../data/database");

/**
 * Fonction qui permet l'inscription de l'utilisateur (envoie au passage un mail de confirmation)
 * @param {object} user - objet contenant les informations de l'utilisateur (mail,password,passwordVerify)
 * @param {object} res - réponse HTTP
 * @returns {object} réponse HTTP contenant un message si l'inscription a bien eu lieu ou si les champs ont été remplis de manière incorrecte
 */
const addUser = async (user, res) => {
  const securePassword = await hashPass(user.password);

  const reqDB = {
    text: "INSERT INTO USERS (mail,password) VALUES ($1,$2)",
    values: [user.mail, securePassword],
  };
  // Ajoute l'utilisateur dans la BDD , SI erreur: renvoie un message d'erreur SINON on envoie un mail de confirmation et un status 200
  db.getDB().query(reqDB, async (err) => {
    if (err) {
      return res.status(500).json({
        error: "Error DB",
      });
    } else {
      return res.status(200).json({ message: "Registered successfully !" });
    }
  });
};

exports.addUser = addUser;
