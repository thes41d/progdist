const db = require("../data/database");

/**
 * Fonction qui renvoie une réponse pour la suppression d'un utilisateur
 * @param {number} userID - ID de l'utilisateur contenu dans le token
 * @param {object} res - réponse HTTP
 * @returns {objet} contient un attribut message indiquant si le site a bien été supprimé ou non
 */
// Renvoie une réponse si l'utilisateur a été supprimé
const deleteAccount = async (userID, res) => {
  let query;
  const reqDB = {
    text: "DELETE FROM PROJECTS WHERE userid = $1",
    values: [userID],
  };
  const reqDB2 = {
    text: "DELETE FROM USERS WHERE id = $1",
    values: [userID],
  };
  try {
    query = await db
      .getDB()
      .query(reqDB)
      .then((res) =>
        db
          .getDB()
          .query(reqDB2)
          .then((res) => res.rowCount)
      );
  } catch (err) {
    return res.status(500).json({
      error: "Error DB",
    });
  }

  if (query)
    return res.status(200).json({
      message: "The account has been removed successfully",
    });
  else
    return res.status(500).json({
      error: "An error occured while deleting your account",
    });
};

exports.deleteAccount = deleteAccount;
