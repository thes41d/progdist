const { getUser } = require("../helpers/getUser");
const {
  validateFormatMail,
  validateFormatPassword,
} = require("../utils/validateFormat");

const registerValidation = async (req, res, next) => {
  let infoDB, alreadyExist;
  const user = req.body;
  try {
    infoDB = await getUser(user.mail); // Récupère les infos de l'utilisateur depuis la BDD (afin de vérifier qu'il n'existe pas déjà)
    alreadyExist = infoDB.rowCount; // Récupère le nombre de ligne renvoyé par le SGBD (O si l'utilisateur n'existe pas 1 sinon)
  } catch (err) {
    console.log(err);
    return res.status(500).json({
      error: "Error DB",
    });
  }
  // Vérifie que les champs ne sont pas null ou undefined
  if (!user.password || !user.passwordVerify || !user.mail) {
    return res.status(400).json({ error: "All fields must be filled in" });
  }

  if (user.password !== user.passwordVerify) {
    return res.status(400).json({
      error: "Passwords must be identical !",
    });
  }
  // ValidateFormat renvoie une erreur si le mail ne respecte pas le format => on entre dans le if ::: Sinon il renvoie null => on entre pas dans le if
  if (validateFormatMail(user.mail)) {
    return res.status(400).json({
      error: "The email format is incorrect",
    });
  }
  // Idem
  if (validateFormatPassword(user.password)) {
    return res.status(400).json({
      error: "The password format is incorrect! ( 1 upper case, 1 lower case, 2 numbers and between 8-20 characters ) ",
    });
  }

  // Vérifie si l'utilisateur existe dans la BDD , s'il existe déjà on renvoie un message d'erreur
  if (alreadyExist) {
    return res.status(400).json({
      error: "You are already registered on our site !",
    });
  }
  next();
};

exports.registerValidation = registerValidation;
