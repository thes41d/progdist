const Joi = require("joi");

/**
 * Fonction qui vérifie si le format du mail entré par l'utilisateur est conforme
 * @param {string} mail - mail entré
 * @returns {null|string} renvoie une erreur si le mail ne respecte pas le format, NULL Sinon
 */

const validateFormatMail = (mail) => {
  const schema = Joi.object().keys({
    mail: Joi.string()
      .max(30)
      .email()
      .trim(true)
      .required()
      .regex(/^[\w \- \.]+@[a-z]{3,10}\.[a-z]{2,5}$/),
  });
  return schema.validate({ mail }).error != undefined;
};

/**
 * Fonction qui vérifie si le format du mot de passe entré par l'utilisateur est conforme
 * @param {string} password - mot de passe entré
 * @returns {null|string} renvoie une erreur si le mot de passe ne respecte pas le format, NULL Sinon
 */

const validateFormatPassword = (password) => {
  const schema = Joi.object().keys({
    password: Joi.string()
      .min(8)
      .max(20)
      .required()
      .regex(/^(?=.*[A-Z]+)(?=.*[a-z]+)(?=.*[0-9]{2,}).{8,20}$/)
  });
  return schema.validate({ password }).error != undefined;
};

/**
 * Fonction qui vérifie si le format de l'URL entré par l'utilisateur est conforme
 * @param {string} url - URL à
 * @returns {null|string} renvoie une erreur si l'URL ne respecte pas le format, NULL Sinon
 */

exports.validateFormatPassword = validateFormatPassword;
exports.validateFormatMail = validateFormatMail;
