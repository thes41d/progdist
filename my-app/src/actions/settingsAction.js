import apiCall from "../helpers/apiCall";

// Récupère le mail de l'utilisateur courant grâce à son ID stocké dans le payload du token
export async function getMail(state, setstate) {
  const data = await apiCall("GET", "getmail", null);
  if (data.errorToken) {
    localStorage.clear();
    window.location.href = "/login";
  }
  if (data.error) {
    localStorage.clear();
    window.location.href = "/login";
    return;
  } else {
    setstate({
      ...state,
      mail: data.mail,
    });
  }
}
