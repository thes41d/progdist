import format from "../utils/ValidateForm";
import apiCall from "../helpers/apiCall";

export default async function onSave(state, setstate) {
  // Vérifie que les champs ne sont pas null ou undefined
  if (!state.mail || !state.password || !state.passwordVerify) {
    setstate({
      ...state,
      error: "All fields must be filled in",
    });
    return;
  }
  // Vérifie que le format est correct, Si le format est correct, la fonction renvoie true SINON false
  if (!format.validateMail(state.mail)) {
    setstate({
      ...state,
      error: "The format of the email is incorrect",
      mail: "",
    });
    return;
  }
  // Idem
  if (!format.validatePassword(state.password)) {
    setstate({
      ...state,
      error: "The password format is incorrect! ( 1 upper case, 1 lower case, 2 numbers and between 8-20 characters ) ",
      password: "",
      passwordVerify: "",
    });
    return;
  }
  if (state.password !== state.passwordVerify) {
    setstate({
      ...state,
      error: "Passwords must be identical !",
      password: "",
      passwordVerify: "",
    });
    return;
  }
  const body = {
    mail: state.mail,
    password: state.password,
    passwordVerify: state.passwordVerify,
  };
  const data = await apiCall("POST", "register", body);
  // Si le serveur renvoie un message d'erreur , on l'affiche.
  if (data.error) {
    setstate({
      ...state,
      error: data.error,
    });
  } else {
    setstate({
      mail: "",
      password: "",
      passwordVerify: "",
      error: "",
      message: data.message,
    });
  }
}
