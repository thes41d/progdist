import format from "../utils/ValidateForm";
import apiCall from "../helpers/apiCall";

export default async function onSave(state, setstate) {
  if (!state.mail || !state.password) {
    setstate({
      ...state,
      error: "All fields must be filled in",
    });
    return;
  }
  if (!format.validateMail(state.mail)) {
    setstate({
      ...state,
      error: "The email format is incorrect",
    });
    return;
  }
  if (!format.validatePassword(state.password)) {
    setstate({
      ...state,
      error: "The password format is incorrect! ( 1 upper case, 1 lower case, 2 numbers and between 8-20 characters ) ",
    });
    return;
  }
  // Envoie au serveur les données entrées par l'utilisateur qui renvoie une erreur si les données sont fausses ou incomplète , sinon un JWT
  const body = { mail: state.mail, password: state.password };
  const data = await apiCall("POST", "login", body);

  if (data.error) {
    setstate({
      ...state,
      password: "",
      error: data.error,
    });
  } else {
    setstate({
      ...state,
      password: "",
      error: "",
      isConnected: true,
    });
    // Met le token recu par le serveur dans le local storage
    localStorage.setItem("token", data.token);
  }
}
