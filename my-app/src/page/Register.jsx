import React, { useState } from "react";
import {
  Avatar,
  Button,
  CssBaseline,
  TextField,
  Link,
  Grid,
  Box,
  Typography,
  Container,
} from "@mui/material";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";

import { createTheme, ThemeProvider } from "@mui/material/styles";
import onSave from "../actions/registerAction";
import { Alert } from "@mui/material";

const theme = createTheme();

const Register = () => {
  const [state, setstate] = useState({
    mail: "",
    password: "",
    passwordVerify: "",
    error: "",
    message: "",
  });

  const displayMessage = () => {
    if (state.message)
      return <Alert severity="success">Registered successfully !</Alert>;
    if (state.error) return <Alert severity="error">{state.error}</Alert>;
  };

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          {displayMessage()}
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign Up
          </Typography>
          <Box component="form" noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="mail"
              label="Email Address"
              name="mail"
              autoComplete="email"
              autoFocus
              error={state.error !== ""}
              value={state.mail}
              onChange={(e) => {
                setstate({
                  ...state,
                  mail: e.target.value,
                });
              }}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              error={state.error !== ""}
              value={state.password}
              onChange={(e) => {
                setstate({
                  ...state,
                  password: e.target.value,
                });
              }}
            />

            <TextField
              margin="normal"
              required
              fullWidth
              name="passwordVerify"
              label="Password"
              type="password"
              id="passwordVerify"
              autoComplete="current-password"
              error={state.error !== ""}
              helperText={state.error}
              value={state.passwordVerify}
              onChange={(e) => {
                setstate({
                  ...state,
                  passwordVerify: e.target.value,
                });
              }}
            />

            <Button
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              onClick={() => onSave(state, setstate)}
            >
              Sign Up
            </Button>
            <Grid container>
              <Grid item>
                <Link href="/login" variant="body2">
                  {"Already have an account? Log In"}
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
};

export default Register;
