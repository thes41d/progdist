import React, { useState } from "react";
import {
  Avatar,
  Box,
  Button,
  CssBaseline,
  TextField,
  Link,
  Grid,
  Typography,
  Container,
} from "@mui/material";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import onSave from "../actions/loginAction";

const theme = createTheme();

const Login = () => {
  const [state, setstate] = useState({
    mail: "",
    password: "",
    error: "",
    isConnected: false,
  });
  const renderRedirect = () => {
    if (state.isConnected || localStorage.getItem("token"))
      window.location.href = "/home";
  };

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <div>{renderRedirect()}</div>
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Log In
          </Typography>
          <Box component="form" noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="mail"
              label="Email Address"
              name="mail"
              autoComplete="email"
              autoFocus
              error={state.error !== ""}
              value={state.mail}
              onChange={(e) => {
                setstate({
                  ...state,
                  mail: e.target.value,
                });
              }}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              error={state.error !== ""}
              helperText={state.error}
              value={state.password}
              autoComplete="current-password"
              onChange={(e) => {
                setstate({
                  ...state,
                  password: e.target.value,
                });
              }}
            />

            <Button
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              onClick={() => onSave(state, setstate)}
            >
              Log In
            </Button>
            <Grid container>
              <Grid item>
                <Link href="/register" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
};

export default Login;
