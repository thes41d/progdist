import React, { useState, useEffect } from "react";
import { getMail } from "../actions/settingsAction";
import { useNavigate } from "react-router-dom";
import {
  CssBaseline,
  Container,
  Paper,
  Button,
  Typography,
  TextField,
} from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";

const theme = createTheme();

const Settings = () => {
  let navigate = useNavigate();
  const [state, setstate] = useState({
    mail: "",
  });
  useEffect(() => {
    if (state.mail === "") {
      getMail(state, setstate);
    }
  }, [state]);
  const handleClick = (path) => {
    navigate(path);
  };
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Container component="main" maxWidth="sm" sx={{ mb: 4 }}>
        <Paper
          variant="outlined"
          sx={{ my: { xs: 5, md: 5 }, p: { xs: 5, md: 6 } }}
        >
          <Typography component="h1" variant="h4" align="center">
            My account
          </Typography>

          <TextField
            id="outlined-read-only-input"
            value={state.mail}
            InputProps={{
              readOnly: true,
            }}
          />
          <Button
            sx={{ my: { xs: 1, md: 0 }, p: { xs: 1, md: 2 } }}
            variant="contained"
            onClick={() => handleClick("/settings/confirm")}
          >
            DELETE MY ACCOUNT
          </Button>
        </Paper>
      </Container>
    </ThemeProvider>
  );
};

export default Settings;
