import React, { useState, useEffect } from "react";
import apiCall from "../helpers/apiCall";
import Items from "../components/projects/Items";
import DialogForm from "../components/projects/DialogForm";

const getProjects = async () => {
  return await apiCall("GET", "projects", null);
};

const Projects = () => {
  const [items, setItems] = useState({
    projects: [],
  });

  const onSubmit = async (name) => {
    if (name) {
      await apiCall("POST", "projects", { name });
      const data = await apiCall("GET", "projects", null);
      setItems({
        projects: data.projects,
      });
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      const data = await getProjects();
      setItems({
        projects: data.projects,
      });
    };
    fetchData();
  }, []);

  return (
    <React.Fragment>
      <Items projects={items.projects} state={items} setState={setItems} />
      <DialogForm
        content={"Enter your project's name"}
        title={"Add a project"}
        onSubmit={onSubmit}
      />
    </React.Fragment>
  );
};

export default Projects;
