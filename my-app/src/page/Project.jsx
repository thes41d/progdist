import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import apiCall from "../helpers/apiCall";
import { BarChart } from "../components/project/Barchart";
import { getCsvHeader, parseData } from "../helpers/csvJSON";
import SelectFeature from "../components/project/SelectFeature";
import CSVReader from "../components/project/CSVReader";
import { Alert, Button, Grid } from "@mui/material";

const Project = () => {
  const { id } = useParams();

  const [info, setInfo] = useState({
    message: "",
    error: "",
  });

  const [project, setProject] = useState({
    dataset: "",
    headers: [],
    name: "",
  });

  const [barData, setBarData] = useState({
    xData: [],
    yData: [],
    xFeature: "",
    yFeature: "",
  });

  const getProject = async (id) => {
    return await apiCall("GET", "projects/project/" + id, null);
  };

  useEffect(() => {
    const fetchData = async () => {
      const res = await getProject(id);
      const body = res.project;
      if (res) {
        const { name, dataset, xfeature, yfeature } = body;
        if (dataset) {
          const headers = getCsvHeader(dataset);

          setProject({
            dataset,
            name,
            headers,
          });
          setBarData({
            ...barData,
            xFeature: xfeature,
            yFeature: yfeature,
          });

          loadBarData(xfeature, yfeature, dataset);
        }
      }
    };
    fetchData();
    // eslint-disable-next-line
  }, []);

  const handleFile = (fileContent) => {
    // Chargement du dataset dans le state
    const headers = getCsvHeader(fileContent);
    setProject({
      ...project,
      dataset: fileContent,
      headers,
    });

    loadBarData(headers[0], headers[1], fileContent);
  };

  const loadBarData = (v1, v2, dataset) => {
    const data = parseData(dataset, v1, v2);
    setBarData({
      xData: data.x,
      yData: data.y,
      xFeature: v1,
      yFeature: v2,
    });
  };

  const onSave = async (e) => {
    const res = await apiCall("PUT", "projects/project/" + id, {
      dataset: project.dataset,
      xfeature: barData.xFeature,
      yfeature: barData.yFeature,
    });
    setInfo({
      message: res.message,
      error: res.error,
    });
  };

  return (
    <Grid container>
      <Grid item xs={2}>
        <CSVReader handleFile={handleFile} />
      </Grid>

      {project.headers.length ? (
        <Grid item>
          <SelectFeature
            onSelect={({ feature1, feature2 }) =>
              loadBarData(feature1, feature2, project.dataset)
            }
            headers={project.headers}
            xFeature={barData.xFeature}
            yFeature={barData.yFeature}
          />
        </Grid>
      ) : null}

      {project.dataset && barData.xData && barData.yData ? (
        <Grid item xs={9}>
          <BarChart
            xData={barData.xData}
            yData={barData.yData}
            title={project.name}
            label={barData.xFeature}
          />
          <Button variant="contained" onClick={onSave}>
            Save it
          </Button>
        </Grid>
      ) : null}
      <Grid item xs={2}>
        {info.message && <Alert severity="success">{info.message}</Alert>}
        {info.error && <Alert severity="error">{info.error}</Alert>}
      </Grid>
    </Grid>
  );
};

export default Project;
