import React, { useState } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@material-ui/core";
import { useNavigate } from "react-router-dom";
import apiCall from "../helpers/apiCall";

// Source : https://material-ui.com/components/dialogs/
/**
 * Composant qui permet l'affichage d'une fenêtre d'intéraction permettant à l'utilisateur de confirmer la suppression de son compte ou de l'annuler
 */
const AlertDialog = () => {
  const [open] = useState(true);
  let navigate = useNavigate();
  const handleDeleteAccount = async () => {
    const res = await apiCall("DELETE", "deleteaccount", null);

    // Si pas d'erreur alors redirection
    if (!res.error) {
      localStorage.clear();
      setTimeout(function () {
        document.location.href = "/home";
      }, 250);
    }
  };

  const handleClose = () => {
    navigate(-1);
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Do you want to delete your account ? "}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            If you delete your account all your project will be removed.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            <span
              className="MuiButton-label"
              style={{ color: "red", fontSize: "1em" }}
            >
              CANCEL
            </span>
          </Button>
          <Button onClick={handleDeleteAccount} color="primary">
            <span
              className="MuiButton-label"
              style={{ color: "blue", fontSize: "1em" }}
            >
              DELETE
            </span>
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default AlertDialog;
