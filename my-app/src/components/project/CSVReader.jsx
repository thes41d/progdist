import React from "react";
import ReactFileReader from "react-file-reader";
import { Button } from "@mui/material";

const CSVReader = ({ handleFile }) => {
  const handleFiles = (file) => {
    const reader = new FileReader();
    reader.onload = () => handleFile(reader.result);
    reader.readAsText(file[0]);
  };

  return (
    <ReactFileReader handleFiles={handleFiles} fileTypes={".csv"}>
      <Button variant="contained" component="span">
        Upload
      </Button>
    </ReactFileReader>
  );
};

export default CSVReader;
