import React from "react";
import { Bar } from "react-chartjs-2";
import Chart from "chart.js/auto";

export const BarChart = ({ xData, yData, title, label }) => {
  const data = {
    labels: xData,
    datasets: [
      {
        label,
        backgroundColor: "rgb(255, 99, 132)",
        borderColor: "rgb(255, 99, 132)",
        data: yData,
      },
    ],
  };

  return (
    <React.Fragment>
      <Bar
        data={data}
        options={{
          plugins: {
            title: {
              display: true,
              text: title,
            },
            legend: {
              display: true,
              position: "bottom",
            },
          },
        }}
      />
    </React.Fragment>
  );
};
