import React, { useState } from "react";
import { MenuItem, Select, InputLabel, Grid } from "@mui/material";

const MySelect = ({ defaultSelected, handleChange, name, id, headers }) => (
  <Select
    onChange={(e) => handleChange(e)}
    name={name}
    labelId={name}
    id={id}
    value={defaultSelected}
  >
    {headers
      ? headers.map((feature) => (
          <MenuItem key={feature} value={feature}>
            {feature}
          </MenuItem>
        ))
      : null}
  </Select>
);

const SelectFeature = ({ onSelect, headers, xFeature, yFeature }) => {
  const f1 = xFeature ? xFeature : headers[0];
  const f2 = yFeature ? yFeature : headers[1];

  const [choices, setChoices] = useState({
    feature1: f1,
    feature2: f2,
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    const c = {
      ...choices,
      [name]: value,
    };
    setChoices(c);

    onSelect(c);
  };

  return (
    <Grid container spacing={10}>
      <Grid item>
        <InputLabel id="feature1">Feature 1</InputLabel>
        <MySelect
          defaultSelected={f1}
          handleChange={handleChange}
          name="feature1"
          id="feature1"
          headers={headers}
        />
      </Grid>
      <Grid item>
        <InputLabel id="feature2">Feature 2</InputLabel>
        <MySelect
          defaultSelected={f2}
          handleChange={handleChange}
          name="feature2"
          id="feature2"
          headers={headers}
        />
      </Grid>
    </Grid>
  );
};
export default SelectFeature;
