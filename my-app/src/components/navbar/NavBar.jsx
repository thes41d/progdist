import React from "react";
import {  useNavigate } from "react-router-dom";
import {AppBar, Box, Toolbar, Typography, Button, IconButton} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";

const Navbar = (props) => {
  let navigate = useNavigate();
  const handleClick = (path) => {
    navigate(path);
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          {props.hidden ? null : (
            <IconButton
              onClick={props.dashboard}
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              sx={{ mr: 2 }}
            >
              <MenuIcon />
            </IconButton>
          )}

          <Typography
            variant="h6"
            component="div"
            sx={{ flexGrow: 1 }}
            onClick={() => handleClick("/home")}
          >
            DataBeez
          </Typography>
          {props.sections.map((section) => (
            <Button
              color="inherit"
              key={section.content}
              onClick={() => handleClick(section.path)}
            >
              {section.content}
            </Button>
          ))}
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Navbar;
