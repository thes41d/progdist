import * as React from 'react';
import {
  Box,
  ListItemText,
  ListItemIcon,
  ListItem,
  List,
  Drawer,
} from "@mui/material";
import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';
import Navbar from './NavBar';

import { useNavigate } from 'react-router-dom';

const NavBarConnect = () => {
  const [state, setState] = React.useState({dashboard: false});

  const dashboard_items = ['My projects','My account','Sign out'];
  const dashboard_items_path = ['/projects', '/settings','/home'];
  let navigate = useNavigate();
  const toggleDrawer = open => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, dashboard: open });
  };


  const handleClick = index => {
    if(dashboard_items[index] === 'Sign out'){
      localStorage.clear();
      window.location.href = "/home";
    }
    navigate(dashboard_items_path[index]);
  }
  const list = (
    <Box
      role="presentation"
      onClick={toggleDrawer(false)}
      onKeyDown={toggleDrawer(false)}
    >
      <List>
        {dashboard_items.map((text, index) => (
          <ListItem button key={text} onClick={() => handleClick(index)}>
            <ListItemIcon>
              {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
    </Box>
  );

  return (
    <div>
      <Navbar sections={[]} dashboard={toggleDrawer(true)} hidden={false}/>
        <React.Fragment >
          <Drawer
            anchor={"left"}
            open={state["dashboard"]}
            onClose={toggleDrawer(false)}
          >
            {list}
          </Drawer>
        </React.Fragment>
      
    </div>
  );
}
export default NavBarConnect;
