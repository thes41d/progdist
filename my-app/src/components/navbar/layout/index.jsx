import React from "react";
import NavBarConnected from "../NavBarConnect";
import NavBar from "../NavBar";

// renvoie la NavBar en mode connecté si l'utilisateur est "connecté" sinon renvoie la NavBar 
// Renvoie aussi tout ses fils c'est à dire l'ensemble des composants qu'il englobe (voir ../App)
const section = [

  {
    content: "Log In",
    path: "/login",
  },
  {
    content: "Sign Up",
    path: "/register"
  }
  
  ]
const index = ({ isConnected, children }) => {
  return (
    <div>
      <div className="topnav">
        {isConnected ? <NavBarConnected  /> : <NavBar sections={section} hidden={true}/>}
      </div>
      {children} 
    </div>
  );
};

index.propTypes = {};

export default index;
