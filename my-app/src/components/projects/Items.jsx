import React from "react";
import Item from "./Item";

const Items = ({ projects, state, setState }) => {
  return (
    <React.Fragment>
      {projects
        ? projects.map((project) => (
            <Item
              key={project.projectid}
              project={project}
              state={state}
              setState={setState}
            />
          ))
        : null}
    </React.Fragment>
  );
};
export default Items;
