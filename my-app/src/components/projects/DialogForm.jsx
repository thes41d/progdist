import * as React from "react";
import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";

const DialogForm = ({ content, title, onSubmit }) => {
  const [open, setOpen] = React.useState(false);
  const [form, setForm] = React.useState({
    value: "",
  });
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = async () => {
    setOpen(false);
  };

  const handleSubmit = async (value) => {
    if (value) {
      handleClose();
      onSubmit(value);
    }
  };

  return (
    <div>
      <Button
        variant="outlined"
        onClick={() => handleClickOpen()}
        style={{ position: "fixed" }}
      >
        {title}
      </Button>
      <Dialog open={open} onClose={() => handleClose()}>
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>
          <DialogContentText>{content}</DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="field"
            label={title}
            type="text"
            fullWidth
            variant="standard"
            onChange={(e) => setForm({ value: e.target.value })}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleClose()}>Cancel</Button>
          <Button onClick={() => handleSubmit(form.value)}>Confirm</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default DialogForm;
