import React from "react";
import { ListItem } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import apiCall from "../../helpers/apiCall";
import { useNavigate } from "react-router-dom";
import { Button } from "@mui/material";

const Item = ({ project, state, setState }) => {
  let navigate = useNavigate();
  const handleDeleteProject = async (projectid) => {
    await apiCall("DELETE", "projects/project/" + projectid, null);
    setState({
      projects: state.projects.filter((item) => item.projectid !== projectid),
    });
  };
  const handleClick = (projectID) => {
    navigate("/projects/project/" + projectID);
  };

  return (
    <ListItem alignItems="center">
      <Button onClick={() => handleClick(project.projectid)}>
        {project.name}
      </Button>
      <DeleteIcon
        onClick={() => handleDeleteProject(project.projectid)}
        style={{ marginLeft: "200px" }}
      />
    </ListItem>
  );
};
export default Item;
