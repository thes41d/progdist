export default class Stat {
  constructor(data) {
    this.data = data;
  }
  getMean = () =>
    this.round(this.data.reduce((a, b) => a + b) / this.data.length);
  getMin = () => this.round(Math.min(...this.data));
  getMax = () => this.round(Math.max(...this.data));

  getStandardDeviation() {
    const variance = this.getVariance(this.data);
    if (variance) return this.round(Math.pow(this.getVariance(this.data), 0.5));
    return 0;
  }

  round(number) {
    return Math.round(number * 100) / 100;
  }

  getVariance() {
    if (this.data.length === 0) return 0;
    const mean = this.getMean(this.data);
    let sum = 0;
    this.data.forEach((element) => (sum += Math.pow(element - mean, 2)));
    return this.round(sum / this.data.length);
  }
}
