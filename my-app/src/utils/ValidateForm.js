class ValidateForm {
    // Renvoie vrai si le mail respecte la regex
    validateMail(mail) {
      const regexMail = /^[\w \- .]+@[a-z]{3,10}\.[a-z]{2,5}$/;
      if (regexMail.test(mail)) return true;
      return false;
    }
    // Renvoie vrai si le mot de passe respecte la regex
    validatePassword(password) {
      const regexPassword = /^(?=.*[A-Z]+)(?=.*[a-z]+)(?=.*[0-9]{2,}).{8,20}$/;
      if (regexPassword.test(password)) return true;
      return false;
    }
  
  }
  
  export default new ValidateForm();
  