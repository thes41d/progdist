import Login from "./page/Login"
import Register from "./page/Register";
import Layout from "./components/navbar/layout/index"
import Home from "./page/Home"
import { BrowserRouter as Router, Route , Routes} from "react-router-dom";
import Settings from "./page/Settings";
import AlertDialog from "./components/AlertDialog";
import Projects from "./page/Projects";
import Project from "./page/Project";

function App() {
  
  let token = localStorage.getItem("token"); // Récupère le token stocké dans le localStorage
  return (
    <div className="App">
      <Router>{/* Permet de configurer les routes et d'afficher un composant selon la route*/}
          <Layout isConnected={!!token}> {/* Permet de configurer la Navbar selon la présence d'un token ou non*/}
            <Routes> {/* Permet d'autoriser l'affichage d'un seul composant à la fois */}
              <Route exact path="/home" element={<Home/>} />
              <Route exact path="/register" element={<Register/>}  />
              <Route exact path="/login" element={<Login/>} />
              <Route exact path="/settings" element={<Settings/>} />
              <Route exact path = "/settings/confirm" element={<AlertDialog/>} />
              <Route path = "/projects/project/:id" element={<Project/>}/>
              <Route exact path= "/projects" element={<Projects/>}/>
              <Route path = "/" element={<Home/>}  /> {/* Si l'url ne coincide avec aucune des routes précédentes , on redirife l'utilisateur à /home*/}
            </Routes>
          </Layout>
      </Router>
    </div>
  );
}


export default App;
