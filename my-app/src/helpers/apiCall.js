const API_URL =
  process.env.API_URL && process.env.API_PORT
    ? `${process.env.API_URL}/${process.env.API_PORT}`
    : "http://localhost:8080";

const getHeader = () => {
  return localStorage.getItem("token")
    ? {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      }
    : {
        Accept: "application/json",
        "Content-Type": "application/json",
      };
};
const apiCall = async (method, path, body) => {
  if (method === "POST" || method === "PUT") {
    return POSTorPUT_API(path, body, method);
  }
  if (method === "GET" || method === "DELETE") {
    return GETorDELETE_API(path, method);
  }
};

const POSTorPUT_API = async (path, body, method) => {
  return await fetch(`${API_URL}/${path}`, {
    method: method,
    headers: getHeader(),
    body: JSON.stringify(body),
  }).then((data) => data.json());
};

const GETorDELETE_API = async (path, method) => {
  return await fetch(`${API_URL}/${path}`, {
    method: method,
    headers: getHeader(),
  }).then((data) => data.json());
};

export default apiCall;
