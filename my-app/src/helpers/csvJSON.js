// Récupère un fichier au format csv et renvoie le nom des colonnes et les lignes sous forme d'object javascript
function csvJSON(csv, sep) {
  var lines = csv.split("\n");
  var result = [];
  var headers = lines[0].split(sep);

  for (var i = 1; i < lines.length; i++) {
    var obj = {};
    var currentline = lines[i].split(sep);

    for (var j = 0; j < headers.length; j++) {
      obj[headers[j]] = currentline[j];
    }

    result.push(obj);
  }

  return result;
}

const getSeparator = (csv) => {
  let headers = csv.split("\n")[0];
  if (headers.includes(",")) return ",";
  if (headers.includes(";")) return ";";
  if (headers.includes("\t")) return "\t";
  return null;
};

export const getCsvHeader = (csv) =>
  csv.split("\n")[0].split(getSeparator(csv));

export const parseData = (csvData, xfeature, yfeature) => {
  const data = csvJSON(csvData, getSeparator(csvData));
  const x = filterData(data, xfeature);
  const y = filterData(data, yfeature);

  return { x, y };
};

const filterData = (data, feature) => {
  return data
    .map((element) => element[feature.toString()])
    .filter((element) => element)
    .map((element) => (!isNaN(element) ? parseInt(element) : element));
};
